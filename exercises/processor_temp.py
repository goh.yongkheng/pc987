#!/usr/bin/env python
import pathlib
import time

def get_cpu_temp():
    ans = 0.0
    p = pathlib.Path('/sys/class/thermal/thermal_zone0/temp')
    if p.exists():
        line = p.read_text().strip()
        try:
            ans = float(line)/1000
        except:
            ans = NA

    return ans

def main(N=5):
    for i in range(N):
        temp = get_cpu_temp()
        now = time.ctime()
        print(f"{now}, temp={temp} degree celsius")
        time.sleep(10)

if __name__ == "__main__":
    #main(1000)   # slow program
    main(1)       # short program