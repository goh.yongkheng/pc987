import sys

maxiter = 255

def mandelbrot(x,y):
    c0 = complex(x, y)
    z = 0
    for i in range(maxiter):
        if abs(z) > 2.0:
            break
        z = z *z + c0
    return (i % 4 * 64, i % 8 * 32, i % 16 * 16), i

if __name__ == "__main__":
    assert len(sys.argv) ==3, "Usage: run_job.py <x value> <y value>"
    
    try:
        x,y = float(sys.argv[1]), float(sys.argv[2])
        t, i = mandelbrot(x,y)
    except:
        print("Usage: run_job.py <x value> <y value>")
        print("<x value> and <y value> are numbers")
        raise TypeError

    print((x,y), t, i)
