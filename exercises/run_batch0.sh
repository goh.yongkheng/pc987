#!/usr/bin/bash

echo "Start!"

mkdir -p run01
python run_job.py -2.0 -1.5 > ./run01/output
echo "Done run01"

mkdir -p run02
python run_job.py -1.99412915851272 -1.5 > ./run02/output
echo "Done run02"

mkdir -p run03
python run_job.py -1.9882583170254402 -1.5 > ./run03/output
echo "Done run03"

