# FILE: mp_process_shared_dict.py
import multiprocessing, time, os, random

def task(d):
    d[multiprocessing.current_process().name] = os.getpid()
    time.sleep(random.randint(1,2))

mgr  = multiprocessing.Manager()
pids = mgr.dict()     # create a shared dictionary

jobs = [ multiprocessing.Process(target=task, args=(pids, ))
         for i in range(10)]

[j.start() for j in jobs]
[j.join()  for j in jobs]
print(pids)

