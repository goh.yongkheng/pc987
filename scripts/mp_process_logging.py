# FILE: mp_process_logging.py
import multiprocessing, logging, functools, sys

def task(num, msg):
    name = multiprocessing.current_process().name
    print(msg.format(num=num,name=name))

task1 = functools.partial(task, msg='Task {num}: {name:10s} is farming.')
task2 = functools.partial(task, msg='Task {num}: {name:10s} is mining.') 

multiprocessing.log_to_stderr(logging.DEBUG)
p1 = multiprocessing.Process(target=task1,args=(1,)) # default name
p2 = multiprocessing.Process(target=task1,args=(1,),name='Farmer')
p3 = multiprocessing.Process(target=task1,args=(1,),name='Miner')
p1.start()
p2.start()
p3.start()
