# FILE: ap_pathlib_delete.py
import pathlib

myfolder = pathlib.Path('level1/level2a')
print(f"Is myfolder exists? {myfolder.exists()}")
print(f"Is it a folder? {myfolder.is_dir()}")

file_list = myfolder.glob("*")
for fname in file_list:
    fname.unlink()

myfolder.rmdir()

print(f"Is myfolder exists? {myfolder.exists()} (deleted)")

