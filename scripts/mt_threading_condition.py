# FILE: mt_threading_condition.py
import threading, time

def producer(cond):
    with cond:
        print('Everyone, dinner is ready!')
        cond.notifyAll()
def consumer(cond):
    print('Consumer is hungry')
    with cond:
        cond.wait()
        print('Consumer eating dinner')

cond = threading.Condition()
threading.Thread(target=consumer,args=(cond,)).start();time.sleep(1)
threading.Thread(target=consumer,args=(cond,)).start();time.sleep(1)
threading.Thread(target=producer,args=(cond,)).start()
