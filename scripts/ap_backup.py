# FILE: ap_backup.py
import pathlib, shutil, os

pick = ['.txt', '.md', '.pdf', '.py']
dry_run = True

def make_backup(dir_name, backup_dir, pick):
    p = pathlib.Path(dir_name)
    b = pathlib.Path(backup_dir)
    for src_name in p.rglob('*'):
        dst_name = b.absolute().joinpath(src_name)
        if src_name.suffix.lower() in pick and src_name.is_file():
            cmd0 = f'os.makedirs(dst_name.parent, exist_ok=True)'
            cmd1 = f'shutil.copy("{src_name}", "{dst_name}")'
            print(cmd1)
            if  not dry_run:
                eval(cmd0)
                eval(cmd1)

backup_dir = pathlib.Path.home().joinpath('tmp')
make_backup('.', backup_dir, pick)    
