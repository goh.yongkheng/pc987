# FILE: mt_threading_lock.py
import threading

lock = threading.Lock()

x=0
def step(num):
    global x
    lock.acquire()
    try:
        for i in range(1000000): 
            x= x + num
    finally:
        lock.release()

t1 = threading.Thread(target=step, args=(1,))
t2 = threading.Thread(target=step, args=(-1,))
t1.start(); t2.start()
t1.join();  t2.join()
print(x)
