# FILE: aio_stream_client.py
import asyncio

async def tcp_echo_client():
    reader, writer = await asyncio.open_connection('127.0.0.1', 8888)

    data = await reader.read()
    print(f'Received: {data.decode()!r}')
    print('Close the connection')
    writer.close()
    await writer.wait_closed()

async def main():
    for i in range(10):
        await tcp_echo_client()

asyncio.run(main())
