# FILE: sp_sys_argv.py
import sys

need_help = '-help' in sys.argv
if need_help:
    print()
    print("This is the help menu for sys_argv that")
    print("shows how to pass command line arguments")
    print("If flag '-help' is issued,")
    print("this message will be printed.")
    print()
    print("The arguments are:")
    for i in sys.argv:
        print(i)
