# FILE: mt_threading_subclass_avoid.py

import threading, time
import multiprocessing

def task():
    print('Pretend to do some work')
    time.sleep(2)

t = threading.Thread(target=task);          t.start()
p = multiprocessing.Process(target=task);   p.start()
