# FILE: rmt_execute_script.py
import subprocess


def run_cmd(cmd):
    rtn = subprocess.run(cmd, shell=True, capture_output=True)
    return rtn

cmd1 = 'ssh dummy@172.16.195.133 "mkdir -p /home/dummy/proj/"'
cmd2 = 'scp scripts/sp_run_list.py dummy@172.16.195.133:/home/dummy/proj/'
cmd3 = 'ssh dummy@172.16.195.133 python3 proj/sp_run_list.py'

for cmd in [cmd1, cmd2, cmd3]:
    rtn = run_cmd(cmd)

print(rtn.stdout.decode('utf8'))
