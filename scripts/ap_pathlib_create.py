# FILE: ap_pathlib_create.py
import pathlib

cwd = pathlib.Path('.')
p1 = cwd.joinpath('level1/level2a')
p2 = cwd.joinpath('level1/level2b')
# p.mkdir()
p1.mkdir(exist_ok=True, parents=True)
p2.mkdir(exist_ok=True, parents=True)
for i in range(10):
    p1.joinpath(f'file_{i}.txt').touch()
    p1.joinpath(f'doc_{i}.pdf').touch()
    p2.joinpath(f'figure_{i}.png').touch()

print(list(cwd.glob("*.txt")))
print(list(cwd.rglob("*.png")))
print([f.name for f in cwd.rglob("*.pdf")])
