# FILE: mt_threading_semaphore.py
import threading, requests, time

urls = ['www.google.com', 'www.python.org', 'wikipedia.com',
        'www.pycon.org',  'twitter.com', 'facebook.com']

max_connections = 4
sema = threading.BoundedSemaphore( max_connections )

def request_url(url):
    with sema:
        print(F'Loading URL {url:22s}')
        br = requests.get('http://'+url)
        time.sleep(2)

for url in urls:
    threading.Thread(target=request_url, args=(url,)).start()

