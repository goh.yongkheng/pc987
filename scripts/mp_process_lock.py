# FILE: mp_process_lock.py
import multiprocessing, sys

def task(lock, msg, stream):
    with lock:
        for i in range(100000):
            stream.write(msg)

lock = multiprocessing.Lock()
p1 = multiprocessing.Process(target=task, args=(lock,'A', sys.stdout))
p2 = multiprocessing.Process(target=task, args=(lock,'*', sys.stdout))

p1.start()
p2.start()
p1.join()
p2.join()

