# FILE: mt_threading_Rlock.py
import threading

lock = threading.RLock()   # Lock() will not work

x=0
def step(num):
    global x
    with lock:
        for i in range(1000000): 
            x= x + num

def up_down():
    global x
    with lock:
        up   = step(1)
        down = step(-1)

t1 = threading.Thread(target=up_down)
t1.start(); t1.join()
print(x)
