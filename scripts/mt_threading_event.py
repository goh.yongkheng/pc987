# FILE: mt_threading_event.py
import threading, time, random

whistle = threading.Event()

def racer(num):
    time.sleep( random.randint(1,8)/3 ) 
    print('Racer %d is ready to race.'%num)
    whistle.wait()
    print('Racer %d reach the Finish Line!!'%num)

def blow_whistle():
    print("\n*** LET'S START THE RACE ***\n")
    time.sleep(2)
    whistle.set()

for i in range(5):
    threading.Thread(target=racer, args=(i,)).start()
time.sleep(3)
blow_whistle()
