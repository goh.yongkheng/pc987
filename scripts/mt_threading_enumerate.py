# FILE: mt_threading_enumerate.py
import threading, time, functools, logging, random

def task():
    pause = random.randint(1,6)/2
    logging.debug( 'Pretending to work for %3.1fs.'%pause )
    time.sleep(pause)

logging.basicConfig( level=logging.DEBUG,
    format='[%(levelname)s] (%(threadName)-10s) %(message)s',)

for i in range(3):
    threading.Thread(target=task, daemon=True).start()

main_thread = threading.main_thread()
for t in threading.enumerate():
    if t is not main_thread:  #cannot join main thread
        logging.debug('joining %s', t.getName())
        t.join()
