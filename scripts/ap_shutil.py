# FILE: ap_shutil.py
import pathlib
import shutil

p = pathlib.Path('level1')
src_dir = pathlib.Path('level1/level2b')
dst_dir = pathlib.Path('level1/level2a')

flist = src_dir.rglob("figure_[456].png")
for fname in flist:
    print(f"Copying {fname} to {dst_dir}")
    if not dst_dir.exists():
        dst_dir.mkdir()
    shutil.copy(fname, dst_dir)

print(f"Total files in level1: {len(list(p.rglob('*')))}")
shutil.rmtree(p)

