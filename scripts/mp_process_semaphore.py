# FILE: mp_process_semaphore.py
import multiprocessing, requests, time

urls = ['www.google.com', 'www.python.org', 'wikipedia.com',
        'www.pycon.org',  'twitter.com', 'facebook.com']

max_connections = 4
sema = multiprocessing.BoundedSemaphore( max_connections )

def request_url(url):
    with sema:
        print(f'Loading URL: {url:<22s}')
        br = requests.get('http://'+url)
        time.sleep(2)

for url in urls:
    multiprocessing.Process(target=request_url,args=(url,)).start()
