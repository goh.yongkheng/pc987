# FILE: rmt_copy_files_1.py
import subprocess
import pathlib

def run_cmd(cmd):
    rtn = subprocess.run(cmd, shell=True)
    print(rtn)

pathlib.Path('./proj').mkdir(exist_ok=True)

cmd1 = 'scp dummy@172.16.195.133:/home/dummy/proj/file*.txt proj'
cmd2 = 'rsync -r dummy@172.16.195.133:/home/dummy/proj .'

for cmd in [cmd1, cmd2]:
    run_cmd(cmd)
