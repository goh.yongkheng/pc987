# FILE: mp_process_daemon.py
import multiprocessing, functools, time
def task(delay):
    p = multiprocessing.current_process()
    print('Starting:', p.name, p.pid)
    time.sleep(delay)
    print('Exiting :', p.name, p.pid)

daemon = functools.partial(task, delay = 5)
mortal = functools.partial(task, delay = 1) 

d = multiprocessing.Process(target=daemon,name='Daemon',daemon=True)
m = multiprocessing.Process(target=mortal, name='Mortal')
d.start(); m.start();

msg ='Daemon is alive: {d}  | Mortal is alive: {m}'

print( msg.format(d=d.is_alive(), m=m.is_alive()) )
m.join()
print( msg.format(d=d.is_alive(), m=m.is_alive()) )
d.join()
print( msg.format(d=d.is_alive(), m=m.is_alive()) )
