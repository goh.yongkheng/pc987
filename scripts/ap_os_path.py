# FILE: ap_os_path.py
import os

def fsize(fname):
    return os.path.getsize(fname)

cwd = os.getcwd()
for root, dirs, files in os.walk('.'):
    size_list = [os.path.join(root,f) for f in files]
    dir_name = root.replace('.',cwd)
    dir_size = sum(map(fsize, size_list) ) / 2014

    msg = f"Analysing directory : {dir_name}\n"
    msg+= f" - Total {len(dirs)} subdirectories\n" if dirs else ''
    msg+= f" - Total {len(files)} files ({dir_size} KB)\n"
    print(msg)
