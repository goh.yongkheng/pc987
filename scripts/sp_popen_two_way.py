# FILE: sp_popen_two_way.py
import subprocess

cmd  = 'cat -'  # concatenate to screen
proc = subprocess.Popen( cmd, shell=True, 
    stdin  = subprocess.PIPE,
    stdout = subprocess.PIPE)

input_stream = 'msg send to subprocess.PIPE\n'
proc_output  = proc.communicate(input_stream.encode('utf8'))
print('Output is a tuple of (stdout, stderr).')
print(proc_output)

