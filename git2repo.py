# FILE: git2repo.py
import subprocess
import sys

try:
    comment = sys.argv[1]
except:
    raise Exception("Usage: python git2repo.py <commit comments>")
    
cmd1 = 'git add -A'
cmd2 = f'git commit -m "{comment}"'
cmd3 = 'git push origin master'
subprocess.run(cmd1, shell=True)
subprocess.run(cmd2, shell=True)
subprocess.run(cmd3, shell=True)